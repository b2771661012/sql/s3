-- [SECTION] INSERTING Records/Row

-- Syntax: INSERT INTO <table_name>(columns) VALUES (values);

INSERT INTO artists(name) VALUES ("Rivermaya");

INSERT INTO artists(name) VALUES ("Blackpink");

INSERT INTO artists(name) VALUES("Taylor Swift");

INSERT INTO artists(name) VALUES ("New Jeans");

INSERT INTO artists(name) VALUES ("Bamboo");

INSERT INTO albums(album_title, date_released, artist_id) VALUES ("Trip", "1996-01-01", 1);

INSERT INTO albums(album_title, date_released, artist_id) VALUES ("Trip", "1996-01-01", 1);

INSERT INTO albums(album_title, date_released, artist_id) VALUES ("Born Pink", "2022-09-22", 2);

INSERT INTO albums(album_title, date_released, artist_id) VALUES ("The Album", "2020-10-02", 2);

INSERT INTO albums(album_title, date_released, artist_id) VALUES ("Midnights", "2022-10-21", 3)

INSERT INTO albums(album_title, date_released, artist_id) VALUES ("New Jeans", "2022-08-01", 4);

INSERT INTO albums(album_title, date_released, artist_id) VALUES ("No Water, No Moon", "2012-01-01", 5);



        

-- add 2 rows
INSERT INTO albums(album_title, date_released, artist_id) 
    VALUES ("Light Peace Love", "2005-01-01", 5), 
        ("As the Music Plays", "2005-01-01",5)



-- insert songs

INSERT INTO songs(song_name, length, genre, album_id)
    VALUES ("Snow on the Beach", 450, "Pop", 4),
            ("Anti-Hero", 330, "Pop", 4)


INSERT INTO songs(song_name, length, genre, album_id)
    VALUES ("Snow on the Beach", 450, "Pop", 4),
            ("Anti-Hero", 330, "Pop", 4)

INSERT INTO songs(song_name, length, genre, album_id)
    VALUES ("Masaya", 450, "Jazz", 10),
            ("Mr. Clay", 357, "Jazz", 10),
            ("Noypi", 700, "OPM", 10)

-- add 2 songs per album
INSERT INTO songs(song_name, length, genre, album_id)
    VALUES ("Himala", 450, "OPM", 1),
            ("Kisapmata", 330, "OPM", 1)

INSERT INTO songs(song_name, length, genre, album_id)
    VALUES ("Pink Venom", 450, "Kipop", 2),
            ("Shut Down", 330, "Kipop", 2)

INSERT INTO songs(song_name, length, genre, album_id)
    VALUES ("Pink Venom", 450, "Kipop", 3),
            ("Shut Down", 330, "Kipop", 3)

INSERT INTO songs(song_name, length, genre, album_id)
    VALUES ("How You Like That", 258, "Kipop", 6),
            ("Pretty Savage", 335, "Kipop", 6)

INSERT INTO songs(song_name, length, genre, album_id)
    VALUES ("Attention", 158, "Pop", 7),
            ("Hype Boy", 410, "Pop", 7)

INSERT INTO songs(song_name, length, genre, album_id)
    VALUES ("In Shadow", 258, "Jazz", 8),
            ("Ikot ng Mundo", 335, "Jazz", 8)

INSERT INTO songs(song_name, length, genre, album_id)
    VALUES ("Hallelujah", 258, "Alternative rock", 9),
            ("Truth", 335, "Alternative rock", 9)

INSERT INTO songs(song_name, length, genre, album_id)
    VALUES ("Hallelujah", 258, "Alternative rock", 10),
            ("Truth", 335, "Alternative rock", 10)







-- [SECTION] READ DATA from data base

SELECT * FROM songs;

-- Specify columns that will be shown
SELECT song_name FROM songs;

SELECT song_name, genre FROM songs;

-- To filter the output of the SELECT operation
SELECT * FROM songs WHERE genre = "OPM";
SELECT song_name, length FROM songs WHERE genre = "OPM";

SELECT * FROM albums WHERE artist_id = 3;

-- We can use AND and OR keyword for multiple expressions in the WHERE clause

-- Display the title and length of the OPM songs that are more than 4 minutes
SELECT song_name, length FROM songs WHERE length > 600 AND genre = "OPM";

SELECT song_name, length, genre FROM songs WHERE genre = "OPM" OR genre = "Jazz";

-- [SECTION] UPDATING RECORDS/DATA
-- UPDATE <table_name> SET <column_name> = <value to be>

UPDATE songs SET length = 428 Where song_name = "Attendtion";


UPDATE songs SET genre = "Original Pinoy Music" Where genre = "OPM";

UPDATE songs SET genre = "Pop" Where genre is NULL;

-- [SECTION] DELETING RECORD
-- DELETE FROM <table_name> Where <condition>;

-- Delete all OPM songs that are more than 4 minutes 
DELETE FROM songs WHERE genre = "Original Pinoy Music"
    AND length > 400;

-- DELETE POP SONGS THAT ARE GREATER THAN 5 mins

DELETE FROM songs WHERE genre = "Pop" AND length < 200



